FROM ubuntu:22.04
LABEL maintainer="edortta71@gmail.com"

ENV DEBIAN_FRONTEND=noninteractive
RUN apt-get update -y
RUN apt-get install default-jre -y
RUN apt-get install default-jdk -y

RUN apt-get install axel curl wget -y

RUN curl https://mirrors.estointernet.in/apache/maven/maven-3/3.8.5/binaries/apache-maven-3.8.5-bin.tar.gz  > /tmp/maven.tar.gz && \
  cd /tmp/ && \
  tar -zxvf /tmp/maven.tar.gz && rm /tmp/maven.tar.gz && \
  mv /tmp/apache-maven-3.8.5 /opt/

ENV M2_HOME="/opt/apache-maven-3.8.5"
ENV PATH="$M2_HOME/bin:$PATH"

RUN curl "https://download-cdn.jetbrains.com/idea/ideaIC-2022.3.2.tar.gz?_gl=1*3v0wk8*_ga*ODI1MTAzOTYyLjE2NzczNDI3OTY.*_ga_9J976DJZ68*MTY3NzM0Mjc5Ni4xLjEuMTY3NzM0Mjg5My42MC4wLjA.&_ga=2.181760099.571857005.1677342797-825103962.1677342796" > /tmp/intelliJ.tar.gz && \
cd /tmp/ && \
tar -zxvf /tmp/intelliJ.tar.gz && \
rm /tmp/intelliJ.tar.gz && \ 
mv /tmp/idea-IC-223.8617.56 /opt/

ENV INTELIJ_HOME="/opt/idea-IC-223.8617.56"
ENV PATH="$INTELIJ_HOME/bin:$PATH"

RUN apt-get install  openssh-server sudo -y

RUN apt-get install x11-apps -y

RUN apt-get install nano -y

RUN useradd -ms /bin/bash -g root -G sudo -u 1000 user

RUN echo "user:user" | chpasswd

RUN ssh-keygen -A

RUN service ssh start

USER user
WORKDIR /home/user
ENV DISPLAY :0
ENV USER_HOME_DIR /home/user

ENV MAVEN_HOME /opt/apache-maven-3.8.5
ENV MAVEN_CONFIG "$USER_HOME_DIR/.m2"

EXPOSE 22
EXPOSE 8761

