# IntelliJ-container


Because I needed to apply some knowledge from a course on microservices using Java, I had to set up a small laboratory so as not to modify my working machine.

This project creates a Docker and installs JDK, Maven and IntelliJ on it.

As the idea is to use it on a server, access is via SSH and you need to have a X11 server installed on your local computer.

The first time you need to build the image.
```bash
ssh esteban@192.168.2.3 -XC
cd dev/intellij-container
bash ./build.sh
bash ./launch.sh
```

After that, you just can access the server and use it.
```bash
ssh esteban@192.168.2.3 -XC
cd dev/intellij-container
bash ./launch.sh
```

There is a folder called `user` that is mapped to /home/user into the box so is there where you can put all your projects as it will be preserved after quit IntelliJ

